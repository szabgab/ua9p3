
// TODO: the unicode-bidi crate provides the control-character constants, the character classes, and
//       even functionality to help decide paragraph directionality
// use unicode_bidi;

const PARA_SEP: &str = "\u{2029}";
// const LINE_SEP: &str = "\u{2028}";
const LRE: char = '\u{202A}';
const RLE: char = '\u{202B}';
const PDF: char = '\u{202C}';

// For future use, maybe
/*
const LRI : char = '\u{2066}';
const RLI : char = '\u{2067}';
const FSI : char = '\u{2068}';
const PDI : char = '\u{2069}';
*/

struct ParaFinder<'a> {
    text: &'a str,
    cur_pos: usize,
    find_para: fn(&str) -> Option<(&str, usize)>,
}

impl<'a> ParaFinder<'a> {
    fn new(text: &'a str, find_para: fn(&str) -> Option<(&str, usize)>) -> ParaFinder {
        ParaFinder {
            text,
            cur_pos: 0,
            find_para,
        }
    }
    // helper
    fn next_para(&self) -> Option<(&'a str, usize)> {
        (self.find_para)(&self.text[self.cur_pos..])
    }
}

impl<'a> Iterator for ParaFinder<'a> {
    type Item = &'a str;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some((para, skip)) = self.next_para() {
            self.cur_pos += skip;
            Some(para)
        } else {
            None
        }
    }
}

pub fn get_dir_markers(text: &str) -> (char, char) {
    const LTR: (char, char) = (LRE, PDF);
    const RTL: (char, char) = (RLE, PDF);
    for chr in text.chars() {
        match unicode_bidi::bidi_class(chr) {}
    }
}

pub fn find_next_para(text: &str) -> Option<(&str, usize)> {
    let mut line_start = 0;
    let mut line_end;
    let mut empty = true;
    let mut rest = text;
    while line_start < text.len() {
        let split = &rest.find('\n').map(|i| i + 1).unwrap_or(rest.len());
        line_end = line_start + split;
        let line = &text[line_start..line_end];
        if line.trim().is_empty() && !empty {
            return Some((&text[..line_start], line_end));
        } else if !line.trim().is_empty() {
            empty = false;
        }
        // Either line wasn't empty, or the para is still empty
        line_start = line_end;
        rest = &text[line_end..];
    }
    if empty {
        None
    } else {
        // Gone through whole text, couldn't find an empty line
        Some((text, text.len()))
    }
}

struct Explicator<'a> {
    finder: ParaFinder<'a>,
}

impl<'a> Explicator<'a> {
    fn new(text: &'a str, find_para: fn(&str) -> Option<(&str, usize)>) -> Explicator {
        let finder = ParaFinder::new(text, find_para);
        Explicator { finder }
    }
}

impl<'a> Iterator for Explicator<'a> {
    type Item = String;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(para) = self.finder.next() {
            let (open, close) = get_dir_markers(para);
            let p = format!("{}{}{}", open, para, close);
            Some(p)
        } else {
            None
        }
    }
}

pub fn explicate(text: &str) -> String {
    let explicator = Explicator::new(text, find_next_para);
    explicator.collect::<Vec<String>>().join(PARA_SEP)
}

#[cfg(test)]
mod tests {
    #[test]
    fn one_line_ltr() {
        let text = "Hello, world!";
        assert_eq!(::explicate(text), format!("{}{}{}", ::LRE, text, ::PDF));
    }

    #[test]
    fn two_para_ltr() {
        let text1 = "Hello, \n world!\n";
        let text2 = "Goodbye, world!\n";
        let text = vec![text1, text2].join("\n");
        let explicated = vec![text1, text2]
            .iter()
            .map(|x| format!("{}{}{}", ::LRE, x, ::PDF))
            .collect::<Vec<String>>()
            .join(::PARA_SEP);

        assert_eq!(::explicate(&text), explicated)
    }
}
